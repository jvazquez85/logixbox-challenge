DOCKER = $(shell which docker)
BUILD_ARG = $(if $(filter  $(NOCACHE), 1),--no-cache)
BUILD_ARG_BASE = $(if $(filter $(NOCACHEBASE), 1),--no-cache)
NETWORK_NAME?=jvazquez_network
USERID?=$(shell id -u)
GROUPID?=$(shell id -g)
BASE_PATH?="$(shell pwd)"
DB_PORT_LOCAL?=5432
DB_TEST_PORT_LOCAL?=0.0.0.0:6432
DB_IMAGE_LOCAL?="postgres:10.10"
DB_LINK?="jvazquez-db-server:db"
TEST_DIR?=$(shell pwd)
ENV_FILE?=.env
GOPRIVATE?=gitlab.com/jvazquez85/*
RUN_ENV?=development

.PHONY: all bot_image test_image migration_image boiler_image \
protos_image run_database run_test_database run_tests shutdown_test_database \
shutdown_database shutdown_grpc_server run_bot_image

network:
	$(DOCKER) network inspect $(NETWORK_NAME) > /dev/null 2>&1 || $(DOCKER) network create $(NETWORK_NAME)
bot_image:
	$(DOCKER) build $(BUILD_ARG) -f build/package/bot/Dockerfile \
		--build-arg run_env=$(RUN_ENV) \
		--build-arg goprivate=$(GOPRIVATE) \
		-t jvazquez-bot-server .
#		--build-arg gitlab_user=$(gitlab_user) \--build-arg gitlab_password=$(gitlab_password) \

test_image:
	$(DOCKER) build $(BUILD_ARG) \
	--build-arg run_env=$(RUN_ENV) \
	--build-arg goprivate=$(GOPRIVATE) \
	--build-arg gitlab_user=$(gitlab_user) \
	--build-arg gitlab_password=$(gitlab_password) \
	-f build/package/test/Dockerfile \
	-t jvazquez-grpc-test-server .
run_bot_image:
	$(DOCKER) run --rm -d \
  		--name jvazquez-bot-server -p $(GRPC_LOCAL_PORT):8080 \
    	--env-file .env \
    	--network $(NETWORK_NAME) \
    	--link jvazquez-db-server:db \
    	$(GRPC_LOCAL_IMAGE)
run_tests:
	$(DOCKER) run \
    	--rm \
    	--network $(NETWORK_NAME) \
    	--link $(DB_LINK) \
    	--env-file .env.test \
    	-v $(TEST_DIR):/go/src/bitbucket.org/jvazquez-bot-server\
    	-v $(TEST_DIR)/sqlboiler-test.toml:/go/src/bitbucket.org/jvazquez-bot-server/sqlboiler.toml \
    	jvazquez-grpc-test-server
volumes:
	$(DOCKER) volume create jvazquez_db_data
	$(DOCKER) volume create jvazquez_test_db_data