package main

import (
	"io"
	"log"
	"net"
	"os"
)

const defaultServiceAddress string = "0.0.0.0:8080"
const defaultNick string = "jvazquez"

func main() {
	var serviceAddress, userNick string

	if address := os.Getenv("SERVICE_ADDRESS"); len(address) > 0 {
		serviceAddress = address
	} else {
		serviceAddress = defaultServiceAddress
	}

	if name := os.Getenv("DEFAULT_NICKNAME"); len(name) > 0 {
		userNick = name
	} else {
		userNick = defaultNick
	}

	log.Printf("Welcome to bot-client emulator. Connecting to %s with nick %s\n", serviceAddress,
		userNick)

	connection, err := net.Dial("tcp", serviceAddress)

	if err != nil {
		log.Fatal(err)
	}
	defer connection.Close()
	mustCopy(os.Stdout, connection)
}

func mustCopy(dst io.Writer, src io.Reader) {
	if _, err := io.Copy(dst, src); err != nil {
		log.Fatal(err)
	}
}
