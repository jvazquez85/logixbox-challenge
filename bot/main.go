package main

import (
	"bufio"
	"gitlab.com/jvazquez85/logixbox-challenge/bot/helpers"
	"log"
	"net"
	"net/textproto"
)

const defaultAddress string = "0.0.0.0:8080"
const defaultNetwork string = "tcp"
const ircMaxMessageLength int = 512

func main() {
	listener, err := net.Listen(defaultNetwork, defaultAddress)

	if err != nil {
		log.Fatalf("Unnable to listen. %v", err)
	}

	chatStream := make(chan string)
	go evaluateMessage(chatStream)
	for {
		conn, err := listener.Accept()

		if err != nil {
			log.Printf("Error when accepting. .%v\n", err)
			continue
		}

		go receiveMessages(conn, chatStream)
	}
}

func receiveMessages(connection net.Conn, chatStream chan string) {
	defer connection.Close()
	defer close(chatStream)
	reader := textproto.NewReader(bufio.NewReader(connection))
	for {
		status, err := reader.ReadLine()

		if err != nil {
			log.Println("Client closed connection. Bye")
			return
		}
		chatStream <- status
	}
}

func evaluateMessage(chatStream chan string) {
	messageInterpreter := new(helpers.ChatMessage)
	for {
		userMessage := <-chatStream
		if len(userMessage) == 0 {
			log.Printf("No message received")
			return
		}
		messageInterpreter.MessageInterpreter(userMessage)
		log.Printf("%s:%s", messageInterpreter.User, messageInterpreter.Message)
	}
}
