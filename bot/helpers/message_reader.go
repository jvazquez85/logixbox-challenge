package helpers

import "strings"

type ChatMessage struct {
	User    string
	Message string
}

func (c *ChatMessage) MessageInterpreter(message string) {
	if len(message) > 0 {
		receivedMessage := strings.Split(message, ":")
		c.User = receivedMessage[0]
		c.Message = strings.TrimLeft(receivedMessage[1], " ")
	}
}
