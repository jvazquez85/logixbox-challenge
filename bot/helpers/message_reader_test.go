package helpers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMessageInterpreterProperlySplitsMessage(t *testing.T) {
	interpreter := new(ChatMessage)
	interpreter.MessageInterpreter("jorge: hello world")
	assert.Equal(t, interpreter.User, "jorge")
	assert.Equal(t, interpreter.Message, "hello world")
}

func TestMessageInterpreterDoesNothingWithEmptyString(t *testing.T) {
	interpreter := new(ChatMessage)
	interpreter.MessageInterpreter("")
	assert.Equal(t, interpreter.User, "")
	assert.Equal(t, interpreter.Message, "")
}
